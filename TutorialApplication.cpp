/*
-----------------------------------------------------------------------------
Filename:	TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___				 __	__ _ _	_
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
	  |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
	: mPlane(0)
	, mPlaneEnt(0)
	, mPlaneNode(0)
	, mMiniScreen(0)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{

}
//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	// Add light which illuminates all objects in the scene regardless of direction
	mSceneMgr->setAmbientLight(Ogre::ColourValue(0.2f, 0.2f, 0.2f));
 
	// Create a directional light that gives shadows
	Ogre::Light* light = mSceneMgr->createLight("MainLight");

	// Set the position of the directional light
	light->setPosition(20, 80, 50);
 
	// Position the camera
	mCamera->setPosition(60, 200, 70);

	// Look at the position with the camera
	mCamera->lookAt(0,0,0);
 
	// Create a material pointer for the plane
	Ogre::MaterialPtr mat = Ogre::MaterialManager::getSingleton().create("PlaneMat", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);

	// Create a new texture unit state using the grass image
	Ogre::TextureUnitState* tuisTexture = mat->getTechnique(0)->getPass(0)->createTextureUnitState("grass_1024.jpg");
 
	// Create a movable plane object
	mPlane = new Ogre::MovablePlane("Plane");

	// Set the plane at the point of origin at 0, 0, 0
	mPlane->d = 0;

	// Set the plane to Y axis, making it flat
	mPlane->normal = Ogre::Vector3::UNIT_Y;
 
	// Create a mesh using the plane
	Ogre::MeshManager::getSingleton().createPlane("PlaneMesh", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, *mPlane, 120, 120, 1, 1, true, 1, 1, 1, Ogre::Vector3::UNIT_Z);

	// Create an instance of a computer graphic model using the mesh
	mPlaneEnt = mSceneMgr->createEntity("PlaneEntity", "PlaneMesh");

	// Set the plane material
	mPlaneEnt->setMaterialName("PlaneMat");
 
	// Create a node for the plane 
	mPlaneNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();

	// Add the plane to the node
	mPlaneNode->attachObject(mPlaneEnt);
 
	// Create a texture pointer for rendering
	Ogre::TexturePtr rtt_texture = Ogre::TextureManager::getSingleton().createManual("RttTex", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, Ogre::TEX_TYPE_2D, mWindow->getWidth(), mWindow->getHeight(), 0, Ogre::PF_R8G8B8, Ogre::TU_RENDERTARGET);
 
	// Create a render texture pointer to the target
	Ogre::RenderTexture *renderTexture = rtt_texture->getBuffer()->getRenderTarget();
 
	// Add the camera's viewport to the render texture
	renderTexture->addViewport(mCamera);

	// Clear the viewport of the render texture before rendering every frame
	renderTexture->getViewport(0)->setClearEveryFrame(true);

	// Set the viewport background color of the render texture to black
	renderTexture->getViewport(0)->setBackgroundColour(Ogre::ColourValue::Black);

	// Turn off overlays on the render texture
	renderTexture->getViewport(0)->setOverlaysEnabled(false);

	// Add a listener to of the render texture
	renderTexture->addListener(this);
 
	// Create a mini screen out of a rectangle
	mMiniScreen = new Ogre::Rectangle2D(true);

	// Set the location of the mini screen in the screen with relative coordinates
	mMiniScreen->setCorners(0.5f, -0.5f, 1.0f, -1.0f);

	// Create a bounding box for the mini screen
	mMiniScreen->setBoundingBox(Ogre::AxisAlignedBox::BOX_INFINITE);
 
	// Create a node for the mini screen
	Ogre::SceneNode* miniScreenNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("MiniScreenNode");

	// Add the mini screen to the node
	miniScreenNode->attachObject(mMiniScreen);
 
	// Create a new material pointer for the mini screen
	Ogre::MaterialPtr renderMaterial = Ogre::MaterialManager::getSingleton().create("RttMat", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);

	// Create a new technique for the mini screen material
	Ogre::Technique* matTechnique = renderMaterial->createTechnique();

	// Create a new pass for the technique
	matTechnique->createPass();

	// Set dynamic lighting off so the material will be constant
	renderMaterial->getTechnique(0)->getPass(0)->setLightingEnabled(false);

	// Create a new texture unit state for the material out of the camera's viewport
	renderMaterial->getTechnique(0)->getPass(0)->createTextureUnitState("RttTex");
 
	// Set the mini screen material
	mMiniScreen->setMaterial("RttMat");  
}
//---------------------------------------------------------------------------
void TutorialApplication::createFrameListener(void)
{
	// Call the BaseApplication virtual function because we still want its functionality
	BaseApplication::createFrameListener();
}
//---------------------------------------------------------------------------
void TutorialApplication::destroyScene(void)
{
}
//---------------------------------------------------------------------------
bool TutorialApplication::frameRenderingQueued(const Ogre::FrameEvent& fe)
{
	// Call the BaseApplication virtual function because we still want its functionality
	bool ret = BaseApplication::frameRenderingQueued(fe);

	// Rotate the plane every frame
	mPlaneNode->yaw(Ogre::Radian(fe.timeSinceLastFrame));

	return ret;
}
//---------------------------------------------------------------------------
bool TutorialApplication::keyPressed(const OIS::KeyEvent& ke)
{
	// Call the BaseApplication virtual function because we still want its functionality
	BaseApplication::keyPressed(ke);

	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::keyReleased(const OIS::KeyEvent& ke)
{
	// Call the BaseApplication virtual function because we still want its functionality
	BaseApplication::keyReleased(ke);

	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::mouseMoved(const OIS::MouseEvent& me)
{
	// Call the BaseApplication virtual function because we still want its functionality
	BaseApplication::mouseMoved(me);

	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::mousePressed(const OIS::MouseEvent& me, OIS::MouseButtonID id)
{
	// Call the BaseApplication virtual function because we still want its functionality
	BaseApplication::mousePressed(me, id);

	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::mouseReleased(const OIS::MouseEvent& me, OIS::MouseButtonID id)
{
	// Call the BaseApplication virtual function because we still want its functionality
	BaseApplication::mouseReleased(me, id);

	return true;
}
//---------------------------------------------------------------------------
void TutorialApplication::preRenderTargetUpdate(const Ogre::RenderTargetEvent& evt)
{
	// Hide the mini screen
	mMiniScreen->setVisible(false);
}
//-------------------------------------------------------------------------------------
void TutorialApplication::postRenderTargetUpdate(const Ogre::RenderTargetEvent& evt)
{
	// Show the mini screen
	mMiniScreen->setVisible(true);
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
	int main(int argc, char *argv[])
#endif
	{
		// Create application object
		TutorialApplication app;

		try {
			app.go();
		} catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
			std::cerr << "An exception has occurred: " <<
			e.getFullDescription().c_str() << std::endl;
#endif
		}

		return 0;
	}

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------